const username = process.env.username;
const password = process.env.password;
const hour = Number(process.env.hour);
const minute = Number(process.env.minute);


export { username, password, hour, minute };