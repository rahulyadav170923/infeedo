Setup(commands) : 
    1. npm install
    2. make sure mongodb daemon is running

Run:

    export username = example@gmail.com;
    export password = password;
    export hour = 8; (In 24 hour format) [use to set scheduled time. 8:00 as defined in the assignment]
    export minute = 0; 
    npm start


Add Demodata:
Add below given example object in demodata array in "demodata.js"

Example : 
    {
        name: 'Client2', email: "rahulyadav70923@gmail.com", users: [
            {name: "rahul", email: "trackrahulyadav@gmail.com", timezone: "Asia/Kolkata"}
        ]
    }