'use strict';

import {Client} from "./models";
import {sendemail} from "./mailer";
import schedule from "node-schedule-tz";
import mongoose from "mongoose";
import { hour, minute } from "./config";
import { data } from "./deomdata";

// add options to connect function
var conn = mongoose.connect('mongodb://localhost/infeedo').then(() => {
    mongoose.connection.db.dropDatabase();
    Client.insertMany(data, (err) => {
        if(!err){
            console.log('Database is initialized')
            scheduleemails();
        }
        else
            console.log(err);
    });
}).catch(() => {
    console.log(error)
});

var scheduleemails = () => {
    var rule = new schedule.RecurrenceRule();
    rule.dayOfWeek = [new schedule.Range(0, 6)];
    rule.hour = hour;
    rule.minute = minute;
    console.log(rule);

    Client.find((err, data) => {
        if(!err){
            console.log(data)
            data.map((client) => {
                client['users'].map((user) => {
                    schedule.scheduleJob({...rule, tz: user.timezone}, 
                        () => sendemail(client.name, client.email, user.email));
                })
            })
        }
    });
}