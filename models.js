import mongoose from "mongoose";


const UserSchema = new mongoose.Schema({name: String, email: String, timezone: String});
const ClientSchema = new mongoose.Schema({name: String, email: String, users: [UserSchema]});

const Client = mongoose.model('Client', ClientSchema);


export { Client };