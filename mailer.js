import nodemailer from "nodemailer";
import { username, password } from "./config";


let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: username,
        pass: password
    }
});


const sendemail = (fromname, fromemail, toemail) => {
    let mailOptions = {
        from: fromname + '👥 <'+fromemail+'>',
        to: toemail,
        subject: 'Hello ✔',
        text: 'Good morning',
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('email sent from '+fromemail+' to '+toemail);
    });
}

export { sendemail };